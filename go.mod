module gitlab.com/centros.one/packaging/porter-helm3-centros

go 1.13

require (
	cloud.google.com/go v0.55.0 // indirect
	get.porter.sh/porter v0.23.0-beta.1
	github.com/Azure/go-autorest/autorest v0.10.0 // indirect
	github.com/Masterminds/semver v1.5.0
	github.com/PaesslerAG/gval v1.0.1 // indirect
	github.com/PuerkitoBio/goquery v1.5.1 // indirect
	github.com/ghodss/yaml v1.0.0
	github.com/gobuffalo/envy v1.9.0 // indirect
	github.com/gobuffalo/logger v1.0.3 // indirect
	github.com/gobuffalo/packd v1.0.0 // indirect
	github.com/gobuffalo/packr/v2 v2.7.1
	github.com/googleapis/gnostic v0.5.3 // indirect
	github.com/hashicorp/go-multierror v1.0.0
	github.com/imdario/mergo v0.3.8 // indirect
	github.com/pkg/errors v0.9.1
	github.com/rogpeppe/go-internal v1.5.2 // indirect
	github.com/spf13/cobra v0.0.6
	github.com/stretchr/testify v1.5.1
	github.com/xeipuuv/gojsonpointer v0.0.0-20190905194746-02993c407bfb // indirect
	github.com/xeipuuv/gojsonschema v1.2.0
	gopkg.in/yaml.v2 v2.2.8
	k8s.io/apimachinery v0.19.7
	k8s.io/client-go v0.19.7
)

replace github.com/hashicorp/go-plugin => github.com/carolynvs/go-plugin v1.0.1-acceptstdin
