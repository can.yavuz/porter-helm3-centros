package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/centros.one/packaging/porter-helm3-centros/pkg/helm3"
)

func buildUpgradeCommand(m *helm3.Mixin) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "upgrade",
		Short: "Execute the invoke functionality of this mixin",
		RunE: func(cmd *cobra.Command, args []string) error {
			return m.Upgrade()
		},
	}
	return cmd
}
