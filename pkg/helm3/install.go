package helm3

import (
	"bytes"
	"fmt"
	"io"
	"os/exec"
	"reflect"
	"sort"
	"strings"

	"github.com/pkg/errors"
	yaml "gopkg.in/yaml.v2"
)

// InstallAction - the install action
type InstallAction struct {
	Steps []InstallStep `yaml:"install"`
}

// InstallStep - the individual steps of the install action
type InstallStep struct {
	InstallArguments `yaml:"helm3-centros"`
}

// InstallArguments - the parameters for a step of the install action
type InstallArguments struct {
	Step `yaml:",inline"`

	Namespace  string            `yaml:"namespace"`
	Name       string            `yaml:"name"`
	Chart      string            `yaml:"chart"`
	Version    string            `yaml:"version"`
	Replace    bool              `yaml:"replace"`
	Set        map[string]string `yaml:"set"`
	Values     []string          `yaml:"values"`
	Devel      bool              `yaml:"devel"`
	UpSert     bool              `yaml:"upsert"`
	Idempotent bool              `yaml:"idempotent"`
	Wait       bool              `yaml:"wait"`
}

type yamldata map[string]interface{}

func getCmdOutput(cmd *exec.Cmd) (string, string, error) {
	var outb, errb bytes.Buffer
	cmd.Stdout = &outb
	cmd.Stderr = &errb
	err := cmd.Run()
	return string(outb.Bytes()), string(errb.Bytes()), err
}

func printCmd(cmd *exec.Cmd, out io.Writer) {
	// format the command with all arguments
	prettyCmd := fmt.Sprintf("%s %s", cmd.Path, strings.Join(cmd.Args, " "))
	fmt.Fprintln(out, prettyCmd)
}

func parseYaml(raw string, result *[]yamldata) error {
	// manifest may (and usually will) contain multiple yaml documents; split them apart at the document start indicator ("---")
	parts := strings.Split(raw, "---\n")

	arr := []yamldata{}
	for _, v := range parts {
		if len(v) > 0 {
			var r yamldata
			err := yaml.Unmarshal([]byte(v), &r)
			if err != nil {
				return err
			}
			arr = append(arr, r)
		}
	}

	*result = arr
	return nil
}

func getManifest(m *Mixin, step InstallStep, manifest *[]yamldata) error {
	cmd := m.NewCommand("helm3", "get", "manifest", step.Name)

	if step.Namespace != "" {
		cmd.Args = append(cmd.Args, "--namespace", step.Namespace)
	}

	printCmd(cmd, m.Out)

	stdout, stderr, err := getCmdOutput(cmd)

	if err != nil {
		if strings.Contains(strings.ToLower(stderr), "release: not found") {
			// release not installed, return empty result (but not an error)
			fmt.Fprintln(m.Out, "release not installed")
			*manifest = []yamldata{}
			return nil
		}
		fmt.Fprintln(m.Out, fmt.Sprintf("STDERR: %s", stderr))
		return err
	}

	return parseYaml(stdout, manifest)
}

func getTemplate(m *Mixin, step InstallStep, template *[]yamldata) error {
	cmd := m.NewCommand("helm3", "template", step.Name, step.Chart, "--skip-tests")

	if step.Namespace != "" {
		cmd.Args = append(cmd.Args, "--namespace", step.Namespace)
	}

	if step.Version != "" {
		cmd.Args = append(cmd.Args, "--version", step.Version)
	}

	for _, v := range step.Values {
		cmd.Args = append(cmd.Args, "--values", v)
	}

	cmd.Args = handleSettingChartValuesForInstall(step, cmd)

	printCmd(cmd, m.Out)

	stdout, stderr, err := getCmdOutput(cmd)

	if err != nil {
		// TODO: error handling?
		fmt.Fprintln(m.Out, fmt.Sprintf("STDERR: %s", stderr))
		return err
	}

	return parseYaml(stdout, template)
}

func isManifestEqual(m *Mixin, step InstallStep) (bool, error) {
	var manifest []yamldata
	err := getManifest(m, step, &manifest)
	if err != nil {
		return false, err
	}

	if manifest == nil || len(manifest) == 0 {
		// empty manifest means release is not installed yet; no need to check for changes
		return false, nil
	}

	var template []yamldata
	err = getTemplate(m, step, &template)
	if err != nil {
		return false, err
	}

	return reflect.DeepEqual(manifest, template), nil
}

// Install - called by the mixin to execute the 'install' command
func (m *Mixin) Install() error {

	payload, err := m.getPayloadData()
	if err != nil {
		return err
	}

	kubeClient, err := m.getKubernetesClient("/root/.kube/config")
	if err != nil {
		return errors.Wrap(err, "couldn't get kubernetes client")
	}

	var action InstallAction
	err = yaml.Unmarshal(payload, &action)
	if err != nil {
		return err
	}
	if len(action.Steps) != 1 {
		return errors.Errorf("expected a single step, but got %d", len(action.Steps))
	}
	step := action.Steps[0]

	if step.Idempotent {
		equal, err := isManifestEqual(m, step)
		if err != nil {
			return err
		}
		if equal {
			fmt.Fprintln(m.Out, "Not installing chart because manifest has not changed")
			return nil
		}
	}

	cmd := m.NewCommand("helm3")

	if step.UpSert {
		cmd.Args = append(cmd.Args, "upgrade", "--install", step.Name, step.Chart)

	} else {
		cmd.Args = append(cmd.Args, "install", step.Name, step.Chart)
	}

	if step.Namespace != "" {
		cmd.Args = append(cmd.Args, "--namespace", step.Namespace)
	}

	if step.Version != "" {
		cmd.Args = append(cmd.Args, "--version", step.Version)
	}

	if step.Replace {
		cmd.Args = append(cmd.Args, "--replace")
	}

	if step.Wait {
		cmd.Args = append(cmd.Args, "--wait")
	}

	if step.Devel {
		cmd.Args = append(cmd.Args, "--devel")
	}

	for _, v := range step.Values {
		cmd.Args = append(cmd.Args, "--values", v)
	}

	cmd.Args = handleSettingChartValuesForInstall(step, cmd)

	cmd.Stdout = m.Out
	cmd.Stderr = m.Err

	printCmd(cmd, m.Out)

	// Here where really the command get executed
	err = cmd.Start()
	// Exit on error
	if err != nil {
		return fmt.Errorf("could not execute command: %s", err)
	}
	err = cmd.Wait()
	// Exit on error
	if err != nil {
		return err
	}
	err = m.handleOutputs(kubeClient, step.Namespace, step.Outputs)
	return err
}

// Prepare set arguments
func handleSettingChartValuesForInstall(step InstallStep, cmd *exec.Cmd) []string {
	// sort the set consistently
	setKeys := make([]string, 0, len(step.Set))
	for k := range step.Set {
		setKeys = append(setKeys, k)
	}
	sort.Strings(setKeys)

	for _, k := range setKeys {
		cmd.Args = append(cmd.Args, "--set", fmt.Sprintf("%s=%s", k, step.Set[k]))
	}
	return cmd.Args
}
